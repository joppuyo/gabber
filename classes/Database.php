<?php

Class Database {

  private $pdo;
  
  function __construct($address, $port, $name, $user, $password) {
    $this->pdo = new PDO("mysql:host=$address;port$=$port;dbname=$name;charset=utf8", $user, $password);
    $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function getAllCars() {
    $query = $this->pdo->query("SELECT * FROM car");
    return $query->fetchAll(PDO::FETCH_ASSOC);
  }

  public function getAllReservations() {
    $query = $this->pdo->query("SELECT * FROM reservation JOIN car ON car.id = reservation.car_id");
    return $query->fetchAll(PDO::FETCH_ASSOC);
  }

  public function getReservationsByCar($carId){
      $query = $this->pdo->prepare("SELECT * FROM reservation WHERE car_id = ?");
      $query->bindParam(1, $carId);
      $query->execute();
      return $query->fetchAll(PDO::FETCH_ASSOC);
  }

  public function getCarById($carId){
    $query = $this->pdo->prepare("SELECT * FROM car WHERE id = ?");
    $query->bindParam(1, $carId);
    $query->execute();
    return $query->fetch(PDO::FETCH_ASSOC);
  }

  public function addReservation($carId, $firstName, $lastName, $startDatetime, $endDateTime, $totalPrice){
    $query = $this->pdo->prepare("INSERT INTO reservation (
                                  car_id,
                                  customer_firstname,
                                  customer_lastname,
                                  start_datetime,
                                  end_datetime,
                                  total_price,
                                  status
                                  ) VALUES (
                                  :car_id,
                                  :customer_firstname,
                                  :customer_lastname,
                                  :start_datetime,
                                  :end_datetime,
                                  :total_price,
                                  'reserved'
                                  )");
    $query->bindParam(":car_id", $carId);
    $query->bindParam(":customer_firstname", $firstName);
    $query->bindParam(":customer_lastname", $lastName);
    $query->bindParam(":start_datetime", $startDatetime);
    $query->bindParam(":end_datetime", $endDateTime);
    $query->bindParam(":total_price", $totalPrice);
    $query->execute();
  }

  public function addNewCar($model, $make, $registrationNumber, $seats, $address, $pricePerHour, $longitude, $latitude) {
    $query = $this->pdo->prepare("INSERT INTO car (
                                  registration_number,
                                  make,
                                  model,
                                  seats,
                                  address,
                                  longitude,
                                  latitude,
                                  price_per_hour
                                  ) VALUES (
                                  :registration_number,
                                  :make,
                                  :model,
                                  :seats,
                                  :address,
                                  :longitude,
                                  :latitude,
                                  :price_per_hour
                                  )");
    $query->bindParam(":registration_number", $registrationNumber);
    $query->bindParam(":make", $make);
    $query->bindParam(":model", $model);
    $query->bindParam(":seats", $seats);
    $query->bindParam(":address", $address);
    $query->bindParam(":longitude", $longitude);
    $query->bindParam(":latitude", $latitude);
    $query->bindParam(":price_per_hour", $pricePerHour);
    $query->execute();
  }

  public function editCar($carId, $model, $make, $registrationNumber, $seats, $address, $pricePerHour, $longitude, $latitude) {
    $query = $this->pdo->prepare("UPDATE car SET
                                  registration_number=:registration_number,
                                  make=:make,
                                  model=:model,
                                  seats=:seats,
                                  address=:address,
                                  longitude=:longitude,
                                  latitude=:latitude,
                                  price_per_hour=:price_per_hour
                                  WHERE id=:id");
    $query->bindParam(":id", $carId);
    $query->bindParam(":registration_number", $registrationNumber);
    $query->bindParam(":make", $make);
    $query->bindParam(":model", $model);
    $query->bindParam(":seats", $seats);
    $query->bindParam(":address", $address);
    $query->bindParam(":longitude", $longitude);
    $query->bindParam(":latitude", $latitude);
    $query->bindParam(":price_per_hour", $pricePerHour);
    $query->execute();
  }

  public function deleteCar($carId) {
    $query = $this->pdo->prepare("DELETE FROM car WHERE id = :id");
    $query->bindParam(":id", $carId);
    $query->execute();
  }
}