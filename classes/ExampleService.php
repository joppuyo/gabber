<?php
class ExampleService {
  public function formatToDatetime($date, $time){
    return $date . " " . $time . ":00";
  }
  public function checkIfOverlaps($carReservationStartDatetime, $carReservationEndDatetime, $userReservationsStartDatetime, $userReservationEndDatetime){
    $carReservationPeriod = new \League\Period\Period($carReservationStartDatetime, $carReservationEndDatetime);
    $userReservationPeriod = new \League\Period\Period($userReservationsStartDatetime, $userReservationEndDatetime);
    if ($carReservationPeriod->overlaps($userReservationPeriod)) {
      return true;
    } else {
      return false;
    }
  }
  public function calculatePrice($userReservationStartDatetime, $userReservationEndDatetime, $pricePerHour){
    $userReservationPeriod = new \League\Period\Period($userReservationStartDatetime, $userReservationEndDatetime);
    $reservationSeconds = $userReservationPeriod->getTimestampInterval();
    $reservationHours = $reservationSeconds / 3600;
    return $reservationHours * $pricePerHour;
  }

  public function getAvailableCars($userReservationStartDatetime, $userReservationEndDatetime){
    $app = \Slim\Slim::getInstance();
    $availableCars = [];
    $cars = $app->databaseService->getAllCars();
    foreach ($cars as &$car) {
      // TODO: tee tämä fiksummin
      $reservations = $app->databaseService->getReservationsByCar($car["id"]);
      $numberOfOverlaps = 0;
      foreach ($reservations as $reservation) {
        if ( $this->checkIfOverlaps($reservation["start_datetime"], $reservation["end_datetime"], $userReservationStartDatetime, $userReservationEndDatetime) ){
          $numberOfOverlaps++;
        }
      }
      if ($numberOfOverlaps === 0) {
        $car["totalPrice"] = $app->exampleService->calculatePrice($userReservationStartDatetime, $userReservationEndDatetime, $car["price_per_hour"]);
        array_push($availableCars, $car);
      }
    }
    return $availableCars;
  }

  public function getCoordinates($address){
    $googleMapsApiUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address);
    $json = json_decode(file_get_contents($googleMapsApiUrl));
    $longitude = null;
    $latitude = null;
    if (!empty($json->results)) {
      $longitude = $json->results[0]->geometry->location->lng;
      $latitude = $json->results[0]->geometry->location->lat;
    }
    $output = [];
    $output["longitude"] = $longitude;
    $output["latitude"] = $latitude;
    return $output;
  }
}