<?php
require "classes/ExampleService.php";
class ExampleServiceTest extends PHPUnit_Framework_TestCase {
  public function testCalculatePrice() {
    $exampleService = new ExampleService();
    $expected = "240";
    $userReservationStartDatetime = "2015-10-01 12:00";
    $userReservationEndDatetime = "2015-10-02 12:00";
    $pricePerHour = 10;
    $actual = $exampleService->calculatePrice($userReservationStartDatetime, $userReservationEndDatetime, $pricePerHour);
    $this->assertEquals($expected, $actual);
  }
}