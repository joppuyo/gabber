<?php

// Load all third party libraries
require "vendor/autoload.php";

// Load other required project files
require "config.php";
require "classes/Database.php";
require "classes/ExampleService.php";

// Start session so flash messages work
session_start();

// Configure application
$app = new \Slim\Slim;

// Configure view to use Twig template engine
$app->config("view", New \Slim\Views\Twig());

// Register Twig helper functions
$view = $app->view();
$view->parserExtensions = [
  new \Slim\Views\TwigExtension(),
];

$app->databaseService = function () {
  return new Database(DB_HOST, DB_PORT, DB_NAME, DB_USERNAME, DB_PASSWORD);
};

$app->exampleService = function () {
  return new ExampleService();
};

// Configure application routing
$app->get("/", function() use ($app) {
  $nowObj = \Carbon\Carbon::now();
  $formDefaults["startdate"] = $nowObj->toDateString();
  $formDefaults["starttime"] = $nowObj->addHour()->format("H") . ":00";
  $formDefaults["enddate"] = $nowObj->toDateString();
  $formDefaults["endtime"] = $nowObj->addHour()->format("H") . ":00";

  $app->render("reservationForm.twig", ["defaults" => $formDefaults]);
});

$app->post("/", function() use ($app) {
  // Tee validaatiot tässä...
  $_SESSION["startdate"] = $app->request->post("startdate");
  $_SESSION["starttime"] = $app->request->post("starttime");
  $_SESSION["enddate"] = $app->request->post("enddate");
  $_SESSION["endtime"] = $app->request->post("endtime");
  $app->redirect("selectcar");
});

$app->get("/selectcar", function() use ($app) {
  $userReservationStartDatetime = $app->exampleService->formatToDatetime($_SESSION["startdate"], $_SESSION["starttime"]);
  $_SESSION["startDatetime"] = $userReservationStartDatetime;
  $userReservationEndDatetime = $app->exampleService->formatToDatetime($_SESSION["enddate"], $_SESSION["endtime"]);
  $_SESSION["endDatetime"] = $userReservationEndDatetime;
  $availableCars = $app->exampleService->getAvailableCars($userReservationStartDatetime, $userReservationEndDatetime);
  $app->render("selectCar.twig", ["cars" => $availableCars]);
});

$app->post("/selectcar", function() use ($app) {
  $_SESSION["car"] = $app->databaseService->getCarById($app->request()->post("id"));
  $_SESSION["price"] = $app->exampleService->calculatePrice($_SESSION["startDatetime"], $_SESSION["endDatetime"], $_SESSION["car"]["price_per_hour"]);
  $app->redirect("enterdetails");
});

$app->get("/enterdetails", function() use ($app){
  $app->render("detailsForm.twig");
});

$app->post("/enterdetails", function() use ($app){
  $_SESSION["firstName"] = $app->request()->post("firstname");
  $_SESSION["lastName"] = $app->request()->post("lastname");
  $_SESSION["email"] = $app->request()->post("email");
  $app->redirect("confirm");
});

$app->get("/confirm", function() use ($app){
  $user["firstName"] = $_SESSION["firstName"];
  $user["lastName"] = $_SESSION["lastName"];
  $user["email"] = $_SESSION["email"];
  $reservation["start"] = $_SESSION["startDatetime"];
  $reservation["end"] = $_SESSION["endDatetime"];
  $reservation["price"] = $_SESSION["price"];

  $app->render("confirm.twig",[
    "reservation" => $reservation,
    "car" => $_SESSION["car"],
    "user" => $user
  ]);
});

$app->post("/confirm", function() use ($app){

  $app->databaseService->addReservation(
    $_SESSION["car"]["id"],
    $_SESSION["firstName"],
    $_SESSION["lastName"],
    $_SESSION["startDatetime"],
    $_SESSION["endDatetime"],
    $_SESSION["price"]
  );

  $app->redirect("thankyou");
});

$app->get("/thankyou", function() use ($app){
  session_destroy();
  $app->render("thankyou.twig");
});

$app->get("/admin/reservations", function() use ($app) {
  $reservations = $app->databaseService->getAllReservations();
  $app->render("adminReservations.twig", ["reservations" => $reservations]);
});

$app->get("/admin/cars", function() use ($app) {
  $cars = $app->databaseService->getAllCars();
  $app->render("adminCars.twig", ["cars" => $cars]);
})->name("adminEditCars");

$app->get("/admin/cars/add", function() use ($app) {
  $app->render("adminCarsAdd.twig");
});

$app->post("/admin/cars/add", function() use ($app) {
  $request = $app->request();
  $coordinates = $app->exampleService->getCoordinates($request->post("address"));
  $app->databaseService->addNewCar($request->post("model"), $request->post("make"), $request->post("registration_number"), $request->post("seats"), $request->post("address"), $request->post("price_per_hour"), $coordinates["longitude"], $coordinates["latitude"]);
  $app->flash("success", "Uusi auto lisätty onnistuneesti!");
  $app->redirectTo("adminEditCars");
});

$app->get("/admin/cars/:carId/edit", function($carId) use ($app) {
  $car = $app->databaseService->getCarById($carId);
  $app->render("adminCarsEdit.twig", ['car' => $car]);
})->name("adminEditCar");

$app->post("/admin/cars/:carId/edit", function($carId) use ($app) {
  $request = $app->request();
  $coordinates = $app->exampleService->getCoordinates($request->post("address"));
  $app->databaseService->editCar($carId, $request->post("model"), $request->post("make"), $request->post("registration_number"), $request->post("seats"), $request->post("address"), $request->post("price_per_hour"), $coordinates["longitude"], $coordinates["latitude"]);
  $app->flash("success", "Auton tiedot muokattu onnistuneesti!");
  $app->redirectTo("adminEditCars");
});

$app->get("/admin/cars/:carId/delete", function($carId) use ($app) {
  $app->databaseService->deleteCar($carId);
  $app->flash("success", "Auto poistettu onnistuneesti!");
  $app->redirectTo("adminEditCars");
})->name("adminDeleteCar");

// Start application
$app->run();